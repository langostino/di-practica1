<?php
	//Mira si estás autorizado a entrar, si no lo estás, no se concecta a la base de datos.
	require_once "session_true.php";
	//Devuelve la conexion a BD.
	require_once "inc/conexion_bd.php";

	$id = (isset($_GET["c_id"])? $_GET["c_id"] : null);
	if(!empty($id)){

		//DELETE
		$stmt = $dbh->prepare('DELETE FROM city WHERE id=:id');
		$rows = $stmt->execute(array(':id' => $_GET['c_id']));

		if ($rows == 1) {
			echo "<h3>ELIMINADO CORRECTAMENTE</h3>";
		}else{
			echo "<h3>ERROR AL ELIMINAR</h3>";
		}
	}
?>

<!DOCTYPE html>
<html style="font-family:Arial">
	<head>
		<title>Desarrollo de interfaces</title>
		<link rel="stylesheet" type="text/css" href="css/paginador.css">
	</head>
	<body>

		<h1>Práctica 2_1 - DI</h1>
		<?php
			if(isset($_SESSION['user_name'])){
				echo "<h3>Estás logeado como: ".$_SESSION['user_name']."</h3>";
			}
		?>
		<ul>
			<li>
				<a href="list.php">Ir a listado</a>
			</li>
			<li>
				<a href="logout.php">Cerrar sesión</a>
			</li>
		</ul>

		<nav>
			<form action="form.php" method="POST">
				<input type="submit" name="Nueva" value="nueva"/>
			</form>
		</nav>

		<br><br>
		
		<form method="POST" action="index.php">
			<?php require "list.php" ?>
			<?php require "paginador.php" ?>
		</form>

	</body>
</html>