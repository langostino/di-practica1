<?php
	include "inc/conexion_bd.php";
	require_once "session_true.php";

	// Recogida y definición de variables.

	$id = isset($_POST['ID'])? $_POST['ID'] : null;
	$name = isset($_POST['Name'])? $_POST['Name'] : null;
	$ccode = isset($_POST['ccode'])? $_POST['ccode'] : null;
	$district = isset($_POST['district'])? $_POST['district'] : null;
	$population = isset($_POST['population'])? $_POST['population'] : null;

	// Recogida de acciones para el paginador

	$pagina = (isset($_POST['pagina'])? $_POST['pagina'] : 1);
	$num_registros = (isset($_POST['num_registros'])? $_POST['num_registros'] : 10);
	$primero = (isset($_POST['primero'])? true : false);
	$ultimo = (isset($_POST['ultimo'])? true : false);
	$siguiente = (isset($_POST['siguiente'])? true : false);
	$anterior = (isset($_POST['anterior'])? true : false);
	$mostrar = (isset($_POST['mostrar'])? true : false);

	/*PARÁMETROS DEL BUSCADOR*/
	$params = array();
	$sql="SELECT count(*) FROM city WHERE true";
	$sql_filters = "";

	if(!empty($id)){
		$params[":id"] = $id;
		$sql_filters.=" and id=".$params[":id"];		
	}

	if(!empty($name)){
		$params[":name"] = $name; 
		$sql_filters.=" and name like '%$name%'";
	}

	if(!empty($ccode)){
		$params[":ccode"] = $ccode; 
		$sql_filters.=" and countrycode like '%$ccode%'";
	}

	if(!empty($district)){
		$params[":district"] = $district; 
		$sql_filters.=" and district like '%$district%'"; 
	}

	if(!empty($population)){
		$params[":population"] = $population; 
		$sql_filters.=" and population > '$population'"; 
	}
	$sql .= $sql_filters;


		$stmt = $dbh->prepare($sql);
		$stmt->execute($params);
		$result = $stmt->fetch();
		$total_registros = $result[0];



	// Calculo de acciones
	$paginas = ceil($total_registros/$num_registros);
	if ($primero) $pagina = 1;
	if ($ultimo) $pagina = $paginas;
	if ($siguiente && $pagina<$paginas) $pagina++;
	if ($anterior && $pagina>1) $pagina--;
	if ($mostrar) $pagina = 1;
	echo "Página: ".$pagina." / ".$paginas;
	echo "</br>";
	echo "Registros: ".($pagina-1)*$num_registros." / ".$total_registros;
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/list.css">
	</head>
	<body>
		<script type="text/javascript">
			function eliminar(id) {
				console.log("ID: "+id);
				var r= confirm("¿Seguro que quieres borrar el registro: "+ id +"?");
				if (r == true) {
					console.log("Eliminar: "+id);
					window.location="index.php?c_id="+id;
				}
			}
		</script>

			<fieldset>
				<legend>Buscador</legend>
				ID: <input type="text" name="ID" tabindex="1" value="<?php echo $id ?>" />
				Name: <input type="text" name="Name" tabindex="2" value="<?php echo $name ?>" />
				Country Code: <input type="text" name="ccode" tabindex="3" value="<?php echo $ccode ?>" />
				District: <input type="text" name="district" tabindex="4" value="<?php echo $district ?>" />
				Population: <input type="text" name="population" tabindex="5" value="<?php echo $population ?>" />
				<input type="Submit" name="Buscar" value="Buscar"/>
				<input type="reset" value="Limpiar"/>
			</fieldset>

<table class="datagrid">
<thead>
	<tr>
		<th>ID</th>
		<th>NAME</th>
		<th>COUNTRY CODE</th>
		<th>DISTRICT</th>
		<th>POPULATION</th>
		<th>ACCIONES</th>
	</tr>
</thead>
<tbody>
	<?php
	try{
			$sql = 'SELECT * FROM city WHERE true';
			$sql .= $sql_filters;
			
			//Si el numero de registros (opción en el paginador) es igual a -1, no hace nada y muestra todos los registros.
			if($num_registros!=-1){
				$sql .= " limit ".($num_registros*($pagina-1)).",".$num_registros;
			}
			
			$stmt = $dbh->prepare($sql);
			$stmt->execute($params);
			$datos = $stmt->fetchAll();
			foreach($datos as $rows) : ?>
				
		  <tr>
		    <td>
				<?php echo $rows[0];?>
		    </td>
		    <td>
		    	<?php echo $rows[1];?>
		    </td>
		    <td>
		    	<?php echo $rows[2];?>
		    </td>
		    <td>
		    	<?php echo $rows[3];?>
		    </td>
		    <td>
		    	<?php echo $rows[4];?>
		    </td>
		    <td>
				<a href="form.php?ID=<?php echo $rows['ID']?>"> Modificar </a>
				<button onclick="eliminar(<?php echo $rows['ID'] ?>)">Eliminar</button>
			</td>
		  </tr>

		  <?php 
			endforeach;
		}catch(PDOException $e){
			echo "Error".$e->getMessage();
			
		}
		?>
</tr>
</tbody>
</table>
</br>

	</body>
</html>