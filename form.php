<?php
	include "inc/conexion_bd.php";

	$id = null;
	$name = null;
	$countryCode = null;
	$district = null;
	$population = null;

	//MODIFICAR (con GET desde el index)

	if(isset($_GET['ID']) && !empty($_GET['ID'])){
		try{
			$stmt_c = $dbh->prepare("SELECT * FROM city WHERE id=:id");
			$stmt_c->execute(array(':id' => $_GET['ID']));

			if ($datos = $stmt_c->fetch(PDO::FETCH_ASSOC)) {
				$id = $datos["ID"];
				$name = $datos["Name"];
				$countryCode = $datos["CountryCode"];
				$district = $datos["District"];
				$population = $datos["Population"];
			}
			}catch(PDOException $e){
				echo "Error: ".$e->getMessage();
			}
	}

?>

<html>
<head>
	<meta charset="utf-8">
	<title>FORM</title>
	<link rel="stylesheet" href="css/form.css">
</head>
<body>
	<ul>
		<li><a href="index.php">Volver a Index</a></li>
		<li><a href="list.php">Volver a Listado</a></li>
	</ul>

	<form method="POST" action ="insert_update_form.php">
		<fieldset>
			<legend>Insertar Ciudad</legend>
			ID: <input type="text" name="ID" tabindex="1" readonly value="<?php echo $id ?>" />
			Nombre: <input type="text" name="Name" tabindex="2" value="<?php echo $name ?>"/>
			Country Code: 
			<select name="CountryCode" tabindex="3" value="<?php echo $countryCode ?>"/>
				<?php
				try{
					$stmt = $dbh->query("SELECT DISTINCT CountryCode FROM city");
					while($rows = $stmt->fetch())
						echo "<option value=".$rows[0].">".$rows[0]."</option>";
					}catch(PDOException $e){
					echo "Error".$e->getMessage();
					}
				?>
			</select>
			Population:	<input type="text" name="Population" tabindex="4" value="<?php echo $population ?>"/>
			District: <input type="text" name="District" tabindex="5" value="<?php echo $district ?>"/>

			<input name="insertar" type="submit" value="Insertar"/>
		</fieldset>
	</form>

	<form method="GET" name="formulario2" action="index.php">
		<fieldset>
			<legend>Eliminar Ciudad</legend>
			ID: <input type="text" name="c_id" />
			<button type="submit">Eliminar</button>
		</fieldset>
	</form>
	</body>
</html>