<?php

session_start();
session_destroy();

echo "Sesión finalizada, redirigiendo en 5 segundos...";
?>

<META HTTP-EQUIV="REFRESH" CONTENT="5;URL=/TDI/di-practica1/login.php">

<ul>
	<li>
		<a href="login.php">Iniciar sesión</a>
	</li>
</ul>
